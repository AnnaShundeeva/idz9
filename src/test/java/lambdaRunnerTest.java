import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

public class lambdaRunnerTest {

    @Test
    public void lengthTest() {
        String str = "java";
        Function<String, Integer> f = LambdaDemo.length;
        int res = LambdaRunner.applyFunction(f, str);

        assertEquals(str.length(), res);
    }

    @Test
    public void firstSymbolTest() {
        String str = "java";
        Function<String, Character> f = LambdaDemo.firstSymbol;
        char res = LambdaRunner.applyFunction(f,str);

        assertEquals('j', res);
    }

    @Test
    public void notSpaceTest() {
        String str = "ja va";
        Function<String, Boolean> f = LambdaDemo.notSpace;
        boolean res = LambdaRunner.applyFunction(f,str);

        assertEquals(false, res);
    }

    @Test
    public void quantityWordTest() {
        String str = "j,a,v,a";
        Function<String, Integer> f = LambdaDemo.quantityWord;
        int res = LambdaRunner.applyFunction(f,str);

        assertEquals(4, res);
    }

    @Test
    public void getAgeTest() {
        Human s = new Student("A", "A", "A", 7, Gender.MALE, "a", "a","a");
        Function<Human, Integer> g = LambdaDemo.getAge;
        int res = LambdaRunner.applyFunction(g,s);
        assertEquals(7, res);
    }

    @Test
    public void sameSurnameCheckTest() {
        Human a = new Student("A", "A", "A", 7, Gender.MALE, "a", "a",
                "a");
        Human b = new Student("B", "B", "B", 7, Gender.MALE, "b", "b",
                "b");
        BiFunction<Human, Human, Boolean> g = LambdaDemo.sameSurnameCheck;
        boolean res = LambdaRunner.applyBiFunction(g,a,b);
        assertEquals(false, res);
    }

    @Test
    public void getFIOStringTest() {
        Human s = new Student("A", "A", "A", 7, Gender.MALE, "a", "a","a");
        Function<Human, String> g = LambdaDemo.getFIOString;
        String res = LambdaRunner.applyFunction(g,s);
        assertEquals("A A A", res);
    }

    @Test
    public void plusAgeTest() {
        Human s = new Human("A", "A", "A", 7, Gender.MALE);
        Function<Human, Human> g = LambdaDemo.plusAge;
        Human res = LambdaRunner.applyFunction(g,s);
        Human c = new Human("A", "A", "A", 8, Gender.MALE);
        assertEquals(c, res);
    }

    @Test
    public void youngerMaxAgeCheckTest() {
        Human a = new Student("A", "A", "A", 7, Gender.MALE, "a", "a",
                "a");
        Human b = new Student("B", "B", "B", 7, Gender.MALE, "b", "b",
                "b");
        Human c = new Human("A", "A", "A", 8, Gender.MALE);

        Human[] humans = {a, b, c};
        int maxAge = 9;

        BiFunction<Human[], Integer, Boolean> g = LambdaDemo.youngerMaxAgeCheck;
        boolean res = LambdaRunner.applyBiFunction(g,humans,maxAge);
        assertEquals(true, res);
    }
}
