//Напишите класс LambdaRunner со статическими методами, которые получают на вход лямбда-выражение и подходящий набор
//параметров, применяют это выражение к заданным параметрам и возвращают результат. Сделайте так, чтобы набор этих
// методов был минимально необходимым

import java.util.function.BiFunction;
import java.util.function.Function;

public class LambdaRunner {

    static <T, R> R applyFunction(Function<T, R> function, T arg) {
        return function.apply(arg);
    }

    static <T, U, R> R applyBiFunction(BiFunction<T, U, R> biFunction, T arg1, U arg2) {
        return biFunction.apply(arg1, arg2);
    }
}
