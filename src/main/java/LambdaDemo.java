import java.util.function.BiFunction;
import java.util.function.Function;

//3. Напишите класс LambdaDemo с набором открытых статических неизменяемых полей,
//которым в качестве значений присвоены следующие лямбда-выражения (можно
//использовать ссылки на методы). При необходимости напишите нужные интерфейсы

public class LambdaDemo {

    //для строки символов получить ее длину
    static Function<String, Integer> length = String::length;

    //для строки символов получить ее первый символ, если он существует, или null иначе
    static Function<String, Character> firstSymbol = str -> {
        if (str.isEmpty()) {
            return null;
        } else {
            return str.charAt(0);
        }
    };

    //для строки проверить, что она не содержит пробелов
    static Function<String, Boolean> notSpace = str -> str.indexOf(" ") == -1;

    //слова в строке разделены запятыми, по строке получить количество слов в ней
    static Function<String, Integer> quantityWord = str -> {
        int count = 0;
        for (char letter : str.toCharArray()) {
            if (letter == ',') {
                count++;
            }
        }
        return count + 1;
    };

    //по человеку получить его возраст
    static Function<Human, Integer> getAge = Human::getAge;

    //по двум людям проверить, что у них одинаковая фамилия
    static BiFunction<Human, Human, Boolean> sameSurnameCheck = ((human1, human2) ->
            (human1.getSurname().equals(human2.getSurname())));

    //получить фамилию, имя и отчество человека в виде одной строки (разделитель — пробел)
    static Function<Human, String> getFIOString = human -> human.getSurname() + " " + human.getName() + " " +
            human.getPatronymic();

    //сделать человека старше на один год (по объекту Human создать новый объект)
    static Function<Human, Human> plusAge = human -> new Human(human.getName(), human.getSurname(), human.getPatronymic(),
            human.getAge() + 1, human.getGender());

    //по трем людям и заданному возрасту maxAge проверить, что все три человека моложе maxAge
    static BiFunction<Human[], Integer, Boolean> youngerMaxAgeCheck = (humans, maxAge) -> {
        return humans[0].getAge() < maxAge && humans[1].getAge() < maxAge && humans[2].getAge() < maxAge;
    };
}
