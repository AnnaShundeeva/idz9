import java.util.Objects;

//1. Напишите класс Human с полями фамилия, имя, отчество, возраст и пол (перечисление). В
//классе должны быть конструкторы, геттеры и сеттеры, методы equals и hashCode.

public class Human {

    private String name;
    private String surname;
    private String patronymic;
    private int age;
    private Gender gender;

    public Human(String name, String surname, String patronymic, int age, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(patronymic, human.patronymic) &&
                gender == human.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, age, gender);
    }
}
