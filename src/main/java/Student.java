//2. Напишите производный класс Student с добавленными полями университет, факультет,
//специальность

public class Student extends Human {

    private String university;
    private String faculty;
    private String specialty;

    public Student(String name, String surname, String patronymic, int age, Gender gender, String university, String
            faculty, String specialty) {
        super(name, surname, patronymic, age, gender);
        this.university = university;
        this.faculty = faculty;
        this.specialty = specialty;
    }
}
